const GoogleSpreadSheet = require('google-spreadsheet');
const { promisify } = require('util');

const creds = require('./client_secret.json');

function printProperty(property) {
  console.log(`Name: ${property['título']}`);
  console.log(`Piso: ${property.piso}`);
  console.log(`Disposición: ${property.disposicion}`);
  console.log(`Precio: ${property.precio}`);
}

async function accessSpreadSheet() {
  const doc = new GoogleSpreadSheet('1kJQEv6y6RdNbi8xS5XEpYUQy8OekG0JXDfxKfq_ZTbk');
  await promisify(doc.useServiceAccountAuth)(creds);
  const info = await promisify(doc.getInfo)();
  const sheet = info.worksheets[0];
  console.log(`Title: ${sheet.title}, Rows: ${sheet.rowCount}`);

  const rows = await promisify(sheet.getRows)({
    // Filtering
    query: 'título = Tony',
    offset: 0,
    limit: 10,
    orderby: 'precio'
  });

  rows.forEach(row => {
    // Update
    // row.piso = 6;
    // row.save();

    // List
    // console.log('-----');
    // printProperty(row);
    // console.log('-----');
  });

  // Delete Tony
  // rows[0].del();

  // Access cells
  const cells = await promisify(sheet.getCells)({
    'min-row': 1,
    'max-row': 3,
    'min-col': 1,
    'max-col': 2,
  });
  for (const cell of cells) {
    console.log(`${cell.row} ${cell.col}: ${cell.value}`)
  }
  // Update single cell
  cells[2].value = 'Alexis';
  cells[2].save();
}

accessSpreadSheet();